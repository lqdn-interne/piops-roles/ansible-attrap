# ansible-attrap

## Description

Ansible role to install Attrap_front, Attrap's front-end

## Installation

Install via the requirements.yml file of your ansible playbook.

## Usage

Use via your ansible playbook.

## Role variables

To download the data from your S3 bucket, you need to set the following variables:

```
attrap_front_s3_access_key: ""
attrap_front_s3_secret_key: ""
attrap_front_s3_host: ""
attrap_front_s3_bucket: ""
```

To use the live update functionnality, set the following variable:

```
attrap_front_update_hook_secret: ""
```

## Support

Get help on `matrix://#attrap:laquadrature.net`

## Contributing

Yes please!

## Authors and acknowledgment

By nono and bastien for La Quadrature du Net, 2024

## License

[CeCILL_V2.1-fr](https://cecill.info/licences/Licence_CeCILL_V2.1-fr.html) (see `LICENSE`)
